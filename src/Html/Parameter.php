<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp\Html;

class Parameter
{
    protected $name;
    protected $type;
    protected $in;
    protected $required;

    public function __construct(array $specification)
    {
        $this->name = $specification['name'] ?? 'unnamed';
        $this->type = $specification['schema']['type'] ?? 'untyped';
        $this->in = $specification['in'] ?? 'unspecified';
        $this->required = $specification['required'] ?? false;
    }

    public function asHtml(): string
    {
        return '<div class="parameter method-detail">' . $this->createParameterContent() . '</div>';
    }

    protected function createParameterContent(): string
    {
        return '<b>' . $this->name . '</b>' . $this->createLabels();
    }

    protected function createLabels(): string
    {
        $labels = '';

        $labels .= $this->createLabel('type', $this->type);

        $labels .= $this->createLabel('in-' . $this->in, $this->in);

        if ($this->required) {
            $labels .= $this->createLabel('required', 'required');
        }

        return $labels;
    }

    protected function createLabel(string $type, string $value): string
    {
        return '<div class="small-label param-label-' . $type . '">' . $value . '</div>';
    }
}