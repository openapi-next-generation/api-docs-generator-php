<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp\Html;

class Schema
{
    protected $sampleData;

    public function __construct(
        array $specification,
        bool $skipReadOnlyProperties = false,
        bool $skipWriteOnlyProperties = false
    )
    {
        $this->sampleData = new Property(
            $specification,
            $skipReadOnlyProperties,
            $skipWriteOnlyProperties
        );
    }

    public function asJson(): string
    {
        return json_encode($this->sampleData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
}