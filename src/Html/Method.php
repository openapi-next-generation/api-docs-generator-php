<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp\Html;

class Method
{
    protected $httpMethod;
    protected $path;
    protected $tags;
    protected $parameters = [];
    protected $request;
    protected $responses = [];

    public function __construct(string $httpMethod, string $path, array $specification)
    {
        $this->httpMethod = strtoupper($httpMethod);
        $this->path = $path;
        $this->tags = $specification['tags'] ?? [];

        foreach ($specification['parameters'] ?? [] as $parameter) {
            $this->parameters[] = new Parameter($parameter);
        }

        if (isset($specification['requestBody'])) {
            $this->request = new Request($specification['requestBody']);
        }

        foreach ($specification['responses'] ?? [] as $statusCode => $responseSpecification) {
            $this->responses[] = new Response($statusCode, $responseSpecification);
        }
    }

    public function getSignature(): string
    {
        return $this->httpMethod . $this->path;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function asHtml(): string
    {
        return
            '<div class="method-head method-' . $this->httpMethod . '">' .
            '<div class="http-method">' . $this->httpMethod . '</div>' .
            '<div class="http-path">' . $this->path . '</div>' .
            '</div>' .
            '<div class="method-details">' .
            $this->createParameters() .
            $this->createRequest() .
            $this->createResponses() .
            '</div>'
            ;
    }

    public function asNavigationHtml(): string
    {
        return
            '<div class="method-nav method-' . $this->httpMethod . '">' .
            '<div class="http-method">' . $this->httpMethod . '</div>' .
            '<div class="http-path">' . $this->path . '</div>' .
            '</div>';
    }

    protected function createParameters(): string
    {
        $content = '';

        /* @var $parameter Parameter */
        foreach ($this->parameters as $parameter) {
            $content .= $parameter->asHtml();
        }

        return
            '<div class="parameters">' .
            '<h3 class="parameters">Parameters</h3>' .
            $content .
            '</div>'
            ;
    }

    protected function createRequest(): string
    {
        if ($this->request instanceof Request) {
            return $this->request->asHtml();
        } else {
            return '';
        }
    }

    protected function createResponses(): string
    {
        $content = '';

        /* @var $response Response */
        foreach ($this->responses as $response) {
            $content .= $response->asHtml();
        }

        return
            '<div class="responses">' .
            '<h3>Responses</h3>' .
            $content .
            '</div>'
            ;
    }
}