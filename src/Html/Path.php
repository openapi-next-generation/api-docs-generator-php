<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp\Html;

class Path
{
    protected $methods = [];

    public function __construct(string $path, array $specification)
    {
        foreach ($specification as $httpMethod => $methodSpecification) {
            $this->methods[] = new Method($httpMethod, $path, $methodSpecification);
        }
    }

    public function getMethods(): array
    {
        return $this->methods;
    }
}