<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp\Html;

class Response
{
    protected $statusCode;
    protected $description;
    protected $contentType = 'application/json';
    protected $schema;

    public function __construct(string $statusCode, array $specification)
    {
        $this->statusCode = $statusCode;

        $this->description = $specification['description'] ?? null;

        $this->schema = new Schema($specification['content'][$this->contentType]['schema'] ?? [], false, true);
    }

    public function asHtml(): string
    {
        return '<div class="response method-detail">' . $this->createResponseBlock() . '</div>';
    }

    protected function createResponseBlock(): string
    {
        return '<h4>' . $this->statusCode . '</h4>' .
            $this->createDescription() .
            $this->createTransferBody();
    }

    protected function createDescription(): string
    {
        if ($this->description === null) {
            return '';
        } else {
            return '<h5>Description</h5>' .
                '<div class="response-description">' . $this->description . '</div>';
        }
    }

    protected function createTransferBody(): string
    {
        return '<h5>Response (' . $this->contentType . ')</h5>' .
            '<pre class="transfer-body">' . $this->schema->asJson() . '</pre>';
    }
}