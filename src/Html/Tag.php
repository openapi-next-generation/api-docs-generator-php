<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp\Html;

class Tag
{
    protected $name;
    protected $methods = [];

    public function __construct(array $specification)
    {
        $this->name = $specification['name'] ?? 'default';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addMethod(Method $method): void
    {
        $this->methods[] = $method;
    }

    public function asHtml(): string
    {
        $result = '';

        $result .= '<h2 id="tag-' . $this->name . '">' . $this->name . '</h2>';

        $result .= '<ul class="methods">';
        /* @var $method Method */
        foreach ($this->methods as $method) {
            $result .= '<li id="method-' . $method->getSignature() . '">' . $method->asHtml() . '</li>';
        }
        $result .= '</ul>';

        return $result;
    }

    public function asNavigationHtml(): string
    {
        $result = '';

        $result .= '<a href="#tag-' . $this->name . '"><b>' . $this->name . '</b></a>';

        $result .= '<ul class="methods">';
        /* @var $method Method */
        foreach ($this->methods as $method) {
            $result .= '<li><a href="#method-' . $method->getSignature() . '">' . $method->asNavigationHtml() . '</a></li>';
        }
        $result .= '</ul>';

        return $result;
    }
}