<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp\Html;

class Property implements \JsonSerializable
{
    protected $sampleData;
    protected $skipReadOnlyProperties;
    protected $skipWriteOnlyProperties;

    public function __construct(
        array $specification,
        bool $skipReadOnlyProperties = false,
        bool $skipWriteOnlyProperties = false
    )
    {
        $this->skipReadOnlyProperties = $skipReadOnlyProperties;
        $this->skipWriteOnlyProperties = $skipWriteOnlyProperties;

        if (isset($specification['example'])) {
            $this->sampleData = $specification['example'];
        } else {
            $this->sampleData = $this->createExampleFromStructure($specification);
        }
    }

    public function jsonSerialize()
    {
        return $this->sampleData;
    }

    protected function createExampleFromStructure(array $specification)
    {
        $type = $specification['type'] ?? '';
        switch ($type) {
            case 'object':
                $sampleData = new \stdClass();
                foreach ($specification['properties'] ?? [] as $propertyName => $propertySpecification) {
                    if (
                        ($this->skipReadOnlyProperties && $propertySpecification['readOnly'] ?? false)
                        || ($this->skipWriteOnlyProperties && $propertySpecification['writeOnly'] ?? false)
                    ) {
                        continue;
                    }

                    $sampleData->$propertyName = new Property($propertySpecification);
                }
                break;
            case 'array':
                $sampleData = [
                    new Property($specification['items'] ?? [])
                ];
                break;
            case 'string':
            case 'integer':
            case 'number':
            case 'boolean':
                if (isset($specification['enum'])) {
                    $sampleData = reset($specification['enum']);
                } else {
                    $sampleData = $type;
                }
                break;
            default:
                $sampleData = '';
                break;
        }
        return $sampleData;
    }
}