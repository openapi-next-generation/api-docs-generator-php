<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp\Html;

class Request
{
    protected $contentType = 'application/json';
    protected $schema;

    public function __construct(array $specification)
    {
        $this->schema = new Schema($specification['content'][$this->contentType]['schema'] ?? [], true);
    }

    public function asHtml(): string
    {
        return
            '<div class="request method-detail">' .
            '<h3>Request body</h3>' .
            '<pre class="transfer-body">' . $this->schema->asJson() . '</pre>' .
            '</div>'
            ;
    }
}