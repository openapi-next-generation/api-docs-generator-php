<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp\Html;

use OpenapiNextGeneration\OpenapiRoutesMapperPhp\Route;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\RoutesSpecification;

class Documentation
{
    protected $title;
    protected $tags = [];
    protected $paths = [];

    public function __construct(array $specification)
    {
        $this->title = $specification['info']['title'] ?? 'untitled';

        $routesSpecification = new RoutesSpecification($specification);
        foreach ($routesSpecification->getTags() as $tagName => $methods) {
            if (!isset($this->tags[$tagName])) {
                $this->tags[$tagName] = new Tag(['name' => $tagName]);
            }
            /* @var $route Route */
            foreach ($methods as $route) {
                $this->tags[$tagName]->addMethod(new Method(
                    $route->getHttpMethod(),
                    $route->getPath(),
                    $route->getMethodSpecification()
                ));
            }
        }
    }

    public function asHtml(): string
    {
        return
            '<!DOCTYPE html><html>' .
            '<head>' . $this->createHead() . '</head>' .
            '<body>' . $this->createBody() . '</body>' .
            '</html>';
    }

    protected function createHead(): string
    {
        return
            '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>' .
            '<title>' . $this->title . '</title>' .
            '<style>' . $this->createCss() . '</style>';
    }

    protected function createCss(): string
    {
        return
            'body { font-family: Arial, Helvetica, sans-serif }' .
            'ul { list-style-type: none }' .
            'h1, h2, h3 { border-bottom: 1px #000 solid }' .
            'h3.parameters { margin-top: 0 }' .
            'a, a:visited { color: #000; text-decoration: none }' .
            'a:hover .method-nav { border-left: 3px #000 solid }' .
            '.methods > li { margin-bottom: 2px }' .
            '.http-method, .http-path { display: inline; margin-right: 10px }' .
            '.method-POST { background-color: #49cc90 }' .
            '.method-GET { background-color: #61affe }' .
            '.method-PUT { background-color: #fca130 }' .
            '.method-DELETE { background-color: #f93e3e }' .
            '#navigation { position: fixed; width: 20% }' .
            '#navigation ul { padding-left: 8px }' .
            '.method-nav { border: 2px #444 solid; font-size: 15px; width: 100% }' .
            '#docs { margin-left: 19% }' .
            '.method-head { border: 4px #444 solid; font-size: 30px }' .
            '.method-details { border-left: 4px #444 solid; border-right: 4px #444 solid; padding-left: 10px }' .
            '.response { border-left: 7px #777 solid; padding-left: 10px }' .
            '.transfer-body, .response-description { background-color: #eee; padding: 10px }' .
            '.small-label { border: 1px #000 solid; display: inline; font-size: 12px; font-weight: bold; margin-left: 3px; padding: 1px }' .
            '.param-label-type { background-color: #b8fcb8 }' .
            '.param-label-in-path { background-color: #c0dffe }' .
            '.param-label-in-header { background-color: #fcdeb8 }' .
            '.param-label-required { background-color: #f9aaaa }'
            ;
    }

    protected function createBody(): string
    {
        $result = '';

        $result .= $this->createHeader();

        $result .= '<div>' . $this->createContent() . '</div>';

        return $result;
    }

    protected function createHeader(): string
    {
        return '<h1>' . $this->title . '</h1>';
    }

    protected function createContent(): string
    {
        $result = '';

        $result .= '<nav id="navigation">' . $this->createNavigation() . '</nav>';

        $result .= '<div id="docs">' . $this->createDocs() . '</div>';

        return $result;
    }

    protected function createNavigation(): string
    {
        $result = '<ul class="tags">';
        /* @var $tag Tag */
        foreach ($this->tags as $tag) {
            $result .= '<li>' . $tag->asNavigationHtml() . '</li>';
        }
        $result .= '</ul>';

        return $result;
    }

    protected function createDocs(): string
    {
        $result = '<ul class="tags">';
        /* @var $tag Tag */
        foreach ($this->tags as $tag) {
            $result .= '<li>' . $tag->asHtml() . '</li>';
        }
        $result .= '</ul>';

        return $result;
    }
}