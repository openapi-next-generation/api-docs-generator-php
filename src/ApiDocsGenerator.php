<?php

namespace OpenapiNextGeneration\ApiDocsGeneratorPhp;

use OpenapiNextGeneration\ApiDocsGeneratorPhp\Html\Documentation;

class ApiDocsGenerator
{
    public function buildHtml(array $specification, string $targetFile): void
    {
        $documentation = new Documentation($specification);

        file_put_contents($targetFile, $documentation->asHtml());
    }
}